function bai_1() {
  var diem_chuan = document.getElementById("b1-diem-chuan").value * 1;
  var khu_vuc = document.getElementById("b1-khu-vuc").value * 1;
  var doi_tuong = document.getElementById("b1-doi-tuong").value * 1;

  var diem_1 = document.getElementById("b1-diem-1").value * 1;
  var diem_2 = document.getElementById("b1-diem-2").value * 1;
  var diem_3 = document.getElementById("b1-diem-3").value * 1;

  if (diem_1 == 0 || diem_2 == 0 || diem_3 == 0) {
    document.getElementById("b1-ket-qua").innerText =
      "Bạn đã rớt do có môn điểm bằng 0.";
    return;
  }

  var tong_diem = diem_1 + diem_2 + diem_3 + khu_vuc + doi_tuong;

  if (tong_diem >= diem_chuan) {
    document.getElementById("b1-ket-qua").innerText =
      "Bạn đã đậu. Tổng điểm: " + tong_diem;
  } else {
    document.getElementById("b1-ket-qua").innerText =
      "Bạn đã rớt. Tổng điểm: " + tong_diem;
  }
}

function bai_2() {
  var ho_ten = document.getElementById("b2-ho-ten").value;
  var so_kw = document.getElementById("b2-so-kw").value * 1;
  var so_tien = 0;
  if (so_kw >= 350) {
    so_tien =
      50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (so_kw - 350) * 1300;
  } else if (so_kw >= 200) {
    so_tien = 50 * 500 + 50 * 650 + 100 * 850 + (so_kw - 200) * 1100;
  } else if (so_kw >= 100) {
    so_tien = 50 * 500 + 50 * 650 + (so_kw - 100) * 850;
  } else if (so_kw >= 50) {
    so_tien = 50 * 500 + (so_kw - 50) * 650;
  } else {
    so_tien = so_kw * 500;
  }
  so_tien = new Intl.NumberFormat("vn-VN").format(so_tien);
  document.getElementById("b2-tien-dien").innerText =
    "Họ và tên: " + ho_ten + "; Tiền điện: " + so_tien;
}
